/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bibliotecamultimedia.classes;

/**
 *
 * @author rgcenteno
 */
public class Biblioteca {
    private java.util.Map<Posicion, Multimedia> registro;
    
    public Biblioteca(){
        registro = new java.util.TreeMap<>();        
    }
    
    /**
     * Intenta añadir el elemento en la posición especificada
     * @param m Elemento multimedia a añadir
     * @return true si el elemento se ha añadido, false si no se añade porque la posición está ocupada
     */
    public boolean anadirMultimedia(Multimedia m){
        if(!registro.containsKey(m.getPosicion())){
            registro.put(m.getPosicion(), m);
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean borrarMultimediaEnPosicion(Posicion p){
        if(registro.containsKey(p)){
            registro.remove(p);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(Multimedia m : registro.values()){
            sb.append(m).append("\n");
        }
        return sb.toString();
    }
    
    
}
