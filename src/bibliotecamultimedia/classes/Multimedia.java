/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bibliotecamultimedia.classes;

import com.google.common.base.Preconditions;
/**
 *
 * @author rgcenteno
 */
public abstract class Multimedia {
    private String titulo;
    private int anhoLanzamiento;
    private boolean edicionEspecial;
    private Posicion posicion;

    protected Multimedia(String titulo, int anhoLanzamiento, boolean edicionEspecial, Posicion posicion) {
        Preconditions.checkArgument(anhoLanzamiento <= java.time.LocalDate.now().getYear(), "El año de lanzamiento no puede ser posterior a la fecha actual", anhoLanzamiento);
        Preconditions.checkNotNull(posicion);
        Preconditions.checkNotNull(titulo);
        Preconditions.checkArgument(!titulo.isBlank(), "No se permiten títulos en blanco", titulo);
        this.titulo = titulo;
        this.anhoLanzamiento = anhoLanzamiento;
        this.edicionEspecial = edicionEspecial;
        this.posicion = posicion;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getAnhoLanzamiento() {
        return anhoLanzamiento;
    }

    public boolean isEdicionEspecial() {
        return edicionEspecial;
    }

    public Posicion getPosicion() {
        return posicion;
    }

    public void setPosicion(Posicion posicion) {
        Preconditions.checkNotNull(posicion);
        this.posicion = posicion;
    }
    
    
}
