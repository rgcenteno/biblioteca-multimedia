/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bibliotecamultimedia.classes;

import com.google.common.base.Preconditions;
/**
 *
 * @author rgcenteno
 */
public class Pelicula extends Multimedia{
    
    private String pais;
    private String actorPrincipal;
    private int duracion;

    public Pelicula(String titulo, int anhoLanzamiento, boolean edicionEspecial, Posicion posicion, String pais, String actorPrincipal, int duracion) {
        super(titulo, anhoLanzamiento, edicionEspecial, posicion);
        Preconditions.checkNotNull(pais);
        Preconditions.checkNotNull(actorPrincipal);
        Preconditions.checkArgument(!pais.isBlank());
        Preconditions.checkArgument(!actorPrincipal.isBlank());
        Preconditions.checkArgument(duracion > 0, "No se permiten duraciones menores que cero");
        this.pais = pais;
        this.actorPrincipal = actorPrincipal;
        this.duracion = duracion;
    }

    public String getPais() {
        return pais;
    }

    public String getActorPrincipal() {
        return actorPrincipal;
    }

    public int getDuracion() {
        return duracion;
    }

    @Override
    public String toString() {
        return this.getTitulo() + ", Año lanzamiento: " + this.getAnhoLanzamiento() + ", actorPrincipal: " + actorPrincipal + ", Posición: " + this.getPosicion();
    }
    
    
    
}
