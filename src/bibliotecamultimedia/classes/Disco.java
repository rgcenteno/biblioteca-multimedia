/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bibliotecamultimedia.classes;

import com.google.common.base.Preconditions;

/**
 *
 * @author rgcenteno
 */
public class Disco extends Multimedia{
    private final String discografica;
    private final String grupo;
    private final GeneroMusical generoMusical;

    public Disco(String titulo, int anhoLanzamiento, boolean edicionEspecial, Posicion posicion, String discografica, String grupo, GeneroMusical generoMusical) {
        super(titulo, anhoLanzamiento, edicionEspecial, posicion);
        Preconditions.checkNotNull(grupo);
        Preconditions.checkNotNull(discografica);
        Preconditions.checkNotNull(generoMusical);
        Preconditions.checkArgument(!discografica.isBlank(), "No se permiten valores en blanco");
        Preconditions.checkArgument(!grupo.isBlank(), "No se permiten valores en blanco");
        this.discografica = discografica;
        this.grupo = grupo;
        this.generoMusical = generoMusical;
    }

    public String getDiscografica() {
        return discografica;
    }

    public String getGrupo() {
        return grupo;
    }

    public GeneroMusical getGeneroMusical() {
        return generoMusical;
    }

    @Override
    public String toString() {
        return this.getTitulo() + ": " + discografica + ", grupo=" + grupo + ", generoMusical=" + generoMusical + " Posicion: " + this.getPosicion();
    }
    
    
}
