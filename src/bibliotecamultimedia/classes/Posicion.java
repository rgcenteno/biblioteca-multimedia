/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bibliotecamultimedia.classes;

import com.google.common.base.Preconditions;
import java.util.Objects;
/**
 *
 * @author rgcenteno
 */
public class Posicion implements Comparable<Posicion>{
    private String nombrePasillo;
    private int posicionPasillo;
    private int alturaPasillo;

    public Posicion(String nombrePasillo, int posicionPasillo, int alturaPasillo) {
        Posicion.checkPosicionAltura(posicionPasillo);
        Posicion.checkPosicionAltura(alturaPasillo);
        Preconditions.checkNotNull(nombrePasillo);
        this.nombrePasillo = nombrePasillo;
        this.posicionPasillo = posicionPasillo;
        this.alturaPasillo = alturaPasillo;
    }
    
    public static Posicion of(String posicion){
        Preconditions.checkArgument(posicion.matches("[A-Za-z0-9]+\\-[0-9]*[1-9]+[0-9]*\\-[0-9]*[1-9]+[0-9]*"), "Debe seguir el formato pasillo-alturapasillo-alturaEstanteria", posicion);
        String[] params = posicion.split("-");
        return new Posicion(params[0], Integer.valueOf(params[1]), Integer.valueOf(params[2]));
    }
    
    private static void checkPosicionAltura(int posicion){
        Preconditions.checkArgument(posicion > 0, "No se permiten posiciones menores o iguales que cero", posicion);
    }

    @Override
    public String toString() {
        return nombrePasillo + "-" + posicionPasillo + "-" + alturaPasillo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.nombrePasillo);
        hash = 43 * hash + this.posicionPasillo;
        hash = 43 * hash + this.alturaPasillo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Posicion other = (Posicion) obj;
        if (this.posicionPasillo != other.posicionPasillo) {
            return false;
        }
        if (this.alturaPasillo != other.alturaPasillo) {
            return false;
        }
        if (!Objects.equals(this.nombrePasillo, other.nombrePasillo)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Posicion t) {
        Preconditions.checkNotNull(t);
        if(!this.nombrePasillo.equals(t.nombrePasillo)){
            return this.nombrePasillo.compareTo(t.nombrePasillo);
        }
        else if(this.posicionPasillo != t.posicionPasillo){
            return this.posicionPasillo - t.posicionPasillo;
        }
        else if(this.alturaPasillo != t.alturaPasillo){
            return this.alturaPasillo - t.alturaPasillo;
        }
        else{
            return 0;
        }
    }

    
    
}
