/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecamultimedia;

import bibliotecamultimedia.classes.*;
/**
 *
 * @author rgcenteno
 */
public class BibliotecaMultimedia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Biblioteca b = new Biblioteca();
        b.anadirMultimedia(new Disco("Disco 1", 2020, false, Posicion.of("ALPHA-1-3"), "Discografica 1", "Grupo 1", GeneroMusical.POP));
        b.anadirMultimedia(new Disco("Disco 2", 2022, false, Posicion.of("ALPHA-2-3"), "Discografica 1", "Grupo 1", GeneroMusical.POP));
        b.anadirMultimedia(new Disco("Disco 3", 2019, false, Posicion.of("ALPHA-3-3"), "Discografica 2", "Grupo 2", GeneroMusical.RB));
        b.anadirMultimedia(new Pelicula("Pelicula 1", 1990, false, new Posicion("BETA", 10, 4), "EEUU", "Famosete 1", 120));
        b.anadirMultimedia(new Pelicula("Pelicula 2", 2000, false, new Posicion("ALPHA", 10, 4), "EEUU", "Famosete 2", 110));
        b.anadirMultimedia(new Pelicula("Pelicula 3", 2010, true, new Posicion("ZETA", 11, 4), "EEUU", "Famosete 3", 90));
        System.out.println(b);
        
        b.borrarMultimediaEnPosicion(Posicion.of("ALPHA-2-3"));
        b.borrarMultimediaEnPosicion(Posicion.of("ALPHA-22-3"));
        b.borrarMultimediaEnPosicion(Posicion.of("ZETA-11-4"));
        System.out.println("*****************TRAS BORRADO ***************************");
        System.out.println(b);
    }
    
}
